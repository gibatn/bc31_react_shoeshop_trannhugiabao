import React, { Component } from "react";

export default class Item extends Component {
  render() {
    let { name, image, price } = this.props.shoe;
    return (
      <div className="card col-3" style={{ width: "18rem" }}>
        <img className="card-img-top" src={image} alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <button
            className="btn btn-primary"
            onClick={() => {
              this.props.handleAddToCart(this.props.shoe);
            }}
          >
            Add to cart
          </button>
        </div>
      </div>
    );
  }
}
