import React, { Component } from "react";

export default class Table extends Component {
  renderTable = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr key={item.id}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => {
                this.props.handlePlusSub(item.id, -1);
              }}
              className="btn btn-secondary"
            >
              -
            </button>
            <span className="px-2">{item.num}</span>
            <button
              onClick={() => {
                this.props.handlePlusSub(item.id, 1);
              }}
              className="btn btn-primary"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemove(item.id);
              }}
              className="btn btn-danger"
            >
              Remove
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Ope</th>
            </tr>{" "}
          </thead>
          <tbody>{this.renderTable()}</tbody>
        </table>
      </div>
    );
  }
}
