import React, { Component } from "react";
import Item from "./Item";
import data from "./dataShoe.json";
import Table from "./Table";
export default class ShoeShop extends Component {
  state = {
    shoeArr: data,
    gioHang: [],
  };
  renderShoeItem = () => {
    return this.state.shoeArr.map((item) => {
      return (
        <Item
          key={item.id}
          shoe={item}
          handleAddToCart={this.handleAddToCart}
        />
      );
    });
  };
  handleAddToCart = (shoe) => {
    let cloneshoe = [...this.state.gioHang];

    let index = this.state.gioHang.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let newShoe = { ...shoe, num: 1 };
      cloneshoe.push(newShoe);
    } else {
      cloneshoe[index].num++;
    }
    this.setState({
      gioHang: cloneshoe,
    });
  };
  handleRemove = (idShoe) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };
  handlePlusSub = (idShoe, x) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index !== -1) {
      cloneGioHang[index].num += x;
      this.setState({ gioHang: cloneGioHang });
    }
    if (cloneGioHang[index].num == 0) {
      this.handleRemove(idShoe);
    }
  };
  render() {
    return (
      <div className="container">
        {this.state.gioHang.length > 0 && (
          <Table
            handleRemove={this.handleRemove}
            handlePlusSub={this.handlePlusSub}
            gioHang={this.state.gioHang}
          />
        )}
        <div className="row py-4">{this.renderShoeItem()}</div>
      </div>
    );
  }
}
